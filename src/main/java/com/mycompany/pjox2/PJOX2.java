/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */
package com.mycompany.pjox2;

import java.util.Scanner;

/**
 *
 * @author asus
 */
public class PJOX2 {
     
    static char [][] table = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
    static char player = 'X';
    static int row,col;
    
    public static void main(String[] args){
        printWelcome();
        while (true) {
            printTable();
            printTrun();
            inputRowcol();
            
            if(win()){
                printTable();
                printwin();
                Continue();
                break;
            }else if(Draw()){
                printTable();
                printDraw();
                Continue();
                break;
            }
            swichplayer();
        }
    }
    public static void printWelcome(){
        System.out.println("Welcome to XO");
    }
    private static void printTable() {
        for (int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                System.out.print(table[i][j]+" ");
                
            }
            System.out.println("");
        }
    }
    private static void printTrun(){
        System.out.println(player + " turn");
    }
    private static void inputRowcol(){
        Scanner kb = new Scanner(System.in);
        while(true){
            System.out.println("Please input row,col (row [0-2] and column [0-2]): 0 2");
            row = kb.nextInt();
            col = kb.nextInt();
            if (table[row][col] == '-'){
                table[row][col] = player;
                break;
            }
        }
        
            

        }
    private static void printwin(){
        System.out.println(player+"You Win!!");
    }
    private static  boolean win(){
         if (checkRow()) {
            return true;
        } else if (checkCol()) {
            return true;
        } else if (checkDiagonal()) {
            return true;
        }

        return false;
    }
    private static boolean checkRow(){
        for (int i = 0; i < 3; i++) {
            if (table[row][i] != player) {
                return false;
            }
        }
        return true;
    }
    private static boolean checkCol(){
        for (int i = 0; i < 3; i++) {
            if (table[i][col] != player) {
                return false;
            }
        }
        return true;
    }
    private static boolean checkDiagonal(){
        if (table[0][0] == (player) && table[1][1] == (player) && table[2][2] == (player)) {
            return true;
        }
        if (table[0][2] == (player) && table[1][1] == (player) && table[2][0] == (player)) {
            return false;
        }
        return false;
    }
    private static void swichplayer(){
        if(player == 'X'){
            player = 'O';
        }else {
            player = 'X';
        }
    }
    private static boolean Draw(){
        for (int i =0; i<3; i++){
            for (int j =0; j<3; j++){
            if(table[i][j] == '-'){
                return false;
            }
        }
        }
        return true;
    }
    private static void printDraw(){
        System.out.println("Draw!");
    }
    private static void Continue(){
        Scanner kb = new Scanner(System.in);
        System.out.println("Do you want to play again? (y/n):");
        String playAgain = kb.next();
        if(playAgain.equalsIgnoreCase("y")){
            resettable();
            main(null);
        }else{
            
        }
    }
    private static void resettable(){
        table = new char[][]{{'-','-','-'},{'-','-','-'},{'-','-','-'}};
        player = 'X';
        row = 0;
        col =  0;    }
    
    }
